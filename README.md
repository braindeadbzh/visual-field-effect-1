# Visual Field Effect 1 #
This a demo project for Godot 2.x. It creates a visual field effect in 2D where everything outside the view of the player is turned B&W.

Link to article: https://www.braindead.bzh/entry/godot-quick-tip-3-black-white-visual-field

## Credit for textures ##
Grass: https://opengameart.org/content/seamless-grass-texture-ii
Boulder: https://opengameart.org/content/basic-boulder
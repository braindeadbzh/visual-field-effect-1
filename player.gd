# Contains the player logic
extends KinematicBody2D

const MOTION_SPEED = 350 # Pixels/second

func _ready():
	set_fixed_process(true)
	set_process_input(true)
    
func _fixed_process(delta):
	var motion = Vector2()
	
	if (Input.is_action_pressed("move_up")):
		motion += Vector2(0, -1)
	if (Input.is_action_pressed("move_down")):
		motion += Vector2(0, 1)
	if (Input.is_action_pressed("move_left")):
		motion += Vector2(-1, 0)
	if (Input.is_action_pressed("move_right")):
		motion += Vector2(1, 0)
    
	motion = motion.normalized()*MOTION_SPEED*delta
	move(motion)
    
	if(is_colliding()):
		var collider = get_collider()
		if("box" in collider.get_groups()):
			collider.move(get_collision_normal()*-1)
		else:
			var n = get_collision_normal()
			motion = n.slide(motion)
			move(motion)

	_update_rot()

func _input(event):
	if(event.type == InputEvent.MOUSE_MOTION):
		_update_rot()
   
func _update_rot():
	var mouse_pos = get_global_mouse_pos()
	set_rot(get_pos().angle_to_point(mouse_pos))